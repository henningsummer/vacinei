var CACHE_NAME = 'staticV6';

this.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function(cache) {
            return cache.addAll([
                './',
                './cadastro',
                './cartao',
                './login',
                './postos',
                './vacinas',
                //
                '../css/materialize.min.css',
                '../css/materialize.css',
                '../css/custom.css',
                '../css/style.css',
                '../css/materialdesignicons.css',
                //
                '../fonts/materialdesignicons-webfont.woff2?v=2.1.19',
                '../fonts/roboto/Roboto-Light.woff2',
                '../fonts/roboto/Roboto-Regular.woff2',
                '../fonts/roboto/Roboto-Medium.woff2',
                '../fonts/roboto/Roboto-Medium.woff',
                //
                '../js/jquery-3.2.1.min.js',
                '../js/jquery-form.js',
                '../js/materialize.min.js',
                //'../assets/img/iconappnm.png',
              
                '../assets/img/bg-app.png',
                
                '../assets/img/bebe.png',
                '../assets/img/logobtn.png',
                '../assets/img/logovac.png',
              
                '../assets/img/favicons.ico',
                '../assets/img/postovacina.jpg',
                //
                '/manifest.json',
             ]);
        })
    );
});

self.addEventListener('activate', function activator(event) {
    event.waitUntil(
        caches.keys().then(function(keys) {
            return Promise.all(keys
                .filter(function(key) {
                    return key.indexOf(CACHE_NAME) !== 0;
                })
                .map(function(key) {
                    return caches.delete(key);
                })
            );
        })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request).then(function(cachedResponse) {
            return cachedResponse || fetch(event.request);
        })
    );
});