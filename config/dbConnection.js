/* importar o mongodb */
var mongo = require('mongodb');

var connMongoDB = function(){
	console.log('Conectou com o Banco');
	var db = new mongo.Db(
		'vacineData',
		new mongo.Server(
			'localhost', //string contendo o endereço do servidor
			27017, //porta de conexão
			{}
		),
		{}
	);
	return db;
};

module.exports = function(){
	return connMongoDB;
};