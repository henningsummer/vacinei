//ROTA INICIAL
module.exports = function(mocai){

  mocai.get('/', function (req, res) {//ROTA INICIAL DA APLICAÇÃO
    mocai.app.controllers.indexCT.indexs(mocai, req, res);
  });
  
  mocai.get('/cadastro', function (req, res) { //RENOMEAR ESSA ROTA CONFOME NECESSIDADE
    mocai.app.controllers.indexCT.cadastro(mocai, req, res);
  });

  mocai.get('/postos', function (req, res) { //RENOMEAR ESSA ROTA CONFOME NECESSIDADE
    mocai.app.controllers.indexCT.postos(mocai, req, res);
  });

  mocai.get('/cartao', function (req, res) { //RENOMEAR ESSA ROTA CONFOME NECESSIDADE
    mocai.app.controllers.indexCT.cartao(mocai, req, res);
  });

  mocai.get('/vacinas', function (req, res) { //RENOMEAR ESSA ROTA CONFOME NECESSIDADE
    mocai.app.controllers.indexCT.vacinas(mocai, req, res);
  });
 
  mocai.get('/login', function (req, res) { //RENOMEAR ESSA ROTA CONFOME NECESSIDADE
    mocai.app.controllers.indexCT.login(mocai, req, res);
  });

 

  //OBTENÇÃO DE DADOS
  mocai.post('/enviar/cadastro', function (req, res) {//RENOMEAR ESSA ROTA CONFOME NECESSIDADE
    mocai.app.controllers.indexCT.enviarCadastro(mocai, req, res);
  });
  
};